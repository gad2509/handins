#!/bin/sh

trap killAll TERM

killAll(){
    for j in $(seq 1 $numOfChild); do
        eval kill -s KILL \$child$j
    done
    exit 2
}

[ $# -eq 1 ] || {
    cat <<- USAGE
    Requires only one argument which is number of child processes to be created
    e.g ./waitall.sh 3
USAGE
exit 2
} >&2

uniqueID=$( ps -l | grep waitall.sh | tail -n 2 | head -n 1 | tr -s [:space:] | cut -d " " -f 4 )
success=""
failure=""
numOfChild=$1
echo ""

for i in $(seq 1 $1); do
    ./child.sh $uniqueID $i &
    eval child$i=$!
    i=$(( i+1 ))
done

for i in $(seq 1 $1); do
    if wait $( eval echo \$child$i ); then
        success="$success $( eval echo -n \$child$i )"
    else
        failure="$failure $( eval echo -n \$child$i )"
    fi
done

cat <<-OUTPUT
    Success: $success
    Failures: $failure
OUTPUT

[ -z "$failure" ] && exit 0 || exit 1

