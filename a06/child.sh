#!/bin/sh

trap success USR1

trap failure USR2

success(){
    echo  "Child $uniqueID: success"
    exitStatus=0
    return 0
}

failure(){
    echo "Child $uniqueID: failure"
    exitStatus=1
    return 1
}


#main script
[ $# -eq 0 ] && uniqueID=$( ps -l | grep 'child.sh' | tail -n 1 | tr -s [:space:] | cut -d " " -f 4 ) \
||  uniqueID=$( ps -l | grep ".*$1\b.*child.sh.*" | head -n $2 | tail -n 1 | tr -s [:space:] | cut -d " " -f 4 )

exitStatus=-1

echo "Child $uniqueID: starting"

until [ $exitStatus -ge 0 ]; do
        sleep 1;
done

exit $exitStatus