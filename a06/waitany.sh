#!/bin/sh

trap killAll TERM

killAll(){
    for j in $(seq 1 $numOfChild); do
        eval kill -s KILL \$child$j
    done
    [ $# -eq 0 ] && exit 2 || exit 0
}

[ $# -eq 1 ] || {
    cat <<- USAGE
    Requires only one argument which is number of child processes to be created
    e.g ./waitall.sh 3
USAGE
exit 2
} >&2

uniqueID=$( ps -l | grep waitany.sh | tail -n 2 | head -n 1 | tr -s [:space:] | cut -d " " -f 4 )
success=""
failure=""
numOfChild=$1

for i in $(seq 1 $1); do
    ./child.sh $uniqueID $i &
    eval child$i=$!
    i=$(( i+1 ))
done

wait

for i in $(seq 1 $1); do
    if wait $( eval echo \$child$i ); then
        killAll 0 && exit 0
    fi
done

echo "All failed" && exit 1