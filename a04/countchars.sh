#!/bin/sh

[ $# -ge 1 ] || {
	cat <<- USAGE
	Counts the occurrences of the given character(s) on each line
	Usage: $(basename $0) CHAR1 CHAR2 ...
	
	eg: printf "abc\naabbccc\ndddcc" | ./countchars.sh 'a' 'b' 'c' 'd'
	1 1 1 0
	2 2 3 0
	0 0 2 3	
	USAGE
	exit 2
} >&2

status=0

while IFS="\n" read line; do
	for char; do
		echo -n "$( echo "$line" | tr -dc "$char" | wc -c )"" " || { status=1 && break; }
	done
	[ $status -eq 0 ] || break
	echo ""
done

exit $status
		

	