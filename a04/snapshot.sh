#!/bin/sh

[ -d "./.snapshot" ] || mkdir -v ./.snapshot || exit 1

[ -d "./.snapshot/9" ] && { rm -r -v ./.snapshot/9 || exit 1 ; }

i=8
while [ $i -ge 0 ]; do
	[ -d "./.snapshot/$i" ] && { mv -v ./.snapshot/$i ./.snapshot/$(( i + 1  )) || exit 1 ; }
	i=$((i-1))
done

mkdir -v ./.snapshot/0 && cp -v * ./.snapshot/0 || exit 1

exit 0
