#!/bin/sh

#Incorrect usage of shell
[ $# -ge 1 ]|| {
	cat <<- USAGE
		Displays the lines of input that match all of the given globs
		Usage: $(basename $0) glob...
		
		eg: printf "this\nis\ncrazy\nright"|$(basename $0) '*t*' #line contains a t*
		this
		right	
	USAGE
	exit 2
} >&2
	
#Displays lines that match input globs

status=0;

while IFS="\n" read line; do
	allMatch=0
	for glob; do
		case "$line" in
			$glob) ;;
			*) allMatch=1; break;;
		esac
	done
	
	if [ $allMatch -eq 0 ]; then
		echo "$line"
	fi
done
status2=$?
[ $status2 -ne 0 ]&& status=$status2

exit $status
