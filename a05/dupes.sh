#!/bin/sh

hasDupes(){
	statusReturn=0
	
	[ -f $1 ] && echo -n $1 || { [ $mode -eq 2 ] && return 0; } || { [ $mode -eq 1 ] &&  echo ":$1: is not a file" && return 2; }
	
	for file3; do
		if [ $mode -eq 1 ]; then
			[ -d $file3 ] && statusReturn=2
		fi
		[ -f $file3 ] && [ $1 != $file3 ] && ( cmp -s  $1  $file3 ) \
		&& echo -n ":$file3" && [ $statusReturn -ne 2 ] && statusReturn=1;
	done
	echo ""
	
	return $statusReturn;
}

#main shell
mode=0
tmp=0
exitStatus=0

if [ $# -ge 1 ]; then
	for file; do
		mode=1
		hasDupes "$file" "$@"
		tmp=$?
		[ $tmp -gt $exitStatus ] && exitStatus=$tmp
	done
else 
	for file2 in *; do
		mode=2
		hasDupes "$file2" *
		tmp=$?
		[ $tmp -gt $exitStatus ] && exitStatus=$tmp
	done
fi 

exit $exitStatus

